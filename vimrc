"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set ttyfast

"" Fix backspace indent
set backspace=indent,eol,start

"" Tab options
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab " Change tabs for spaces

"" Visuals
set t_Co=256
set background=dark
set cursorline
set cursorcolumn
let &colorcolumn=join(range(81,999),",")		" highlight column 80
let &colorcolumn=join(range(120,121),",")		" highlight column 120 onward
set ruler										" show the cursor position all the time
set showmatch									" highlight matching braces
set showmode									" show insert/replace/visual mode
set noshowmode									" Hide default --INSERT--
set laststatus=2								" always show status bar
set showcmd										" shows cmd in the bar
set list listchars=tab:>-,trail:.,extends:>,precedes:<,nbsp:+ " Display spaces and tabs

set undofile									" creates Undo file to allow undo after closing a file
set nocompatible								" avoid vi compatibility
set modeline
set modelines=10								" to prevent file exploits
set updatetime=100								" update time (usefull for git diff)
set nowrap										" don't wrap around long lines
set shortmess=a									" avoid 'press enter or...' message on startup
set scrolloff=8                                 " keep 8 lines above and below when scrolling
set noerrorbells
set visualbell
let mapleader = ","								" Leader
let no_buffers_menu=1

" Enable syntax highlighting
syntax enable
syntax on
let python_highlight_all=1

" Line Numbers
set number
set relativenumber
set numberwidth
set lines

" Searching
set ignorecase " Ignore case when searching
set smartcase " When searching try to be smart about cases
set hlsearch " Highlight search results
set incsearch " Makes search act like search in modern browsers

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" Move to brackets with tabs
nnoremap <tab> %
vnoremap <tab> %

" Move by file line and not screen line
nnoremap j gj
nnoremap k gk

" toggle paste mode on/off
set pastetoggle=<F2>

" improved deletion flow
nnoremap <leader>d dd
nnoremap <leader>e ddO

" vim-bbye - closes all buffers maintaining layout
nnoremap <leader>q :Bdelete<CR>

" File cleanup
nnoremap <F5> :%s/\s\+$//e<CR>

" Rope mapping to jump to definition
" Using rename and go to definition from Jedi
" map <leader>j :RopeGotoDefinition
" map <leader>r :RopeRename

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\
if exists("*fugitive#statusline")
	set statusline+=%{fugitive#statusline()}
endif

" Remaps search keys all defaulting to Perl compatible regex
nnoremap / /\v
vnoremap / /\v
map <c-space> ?
nnoremap <leader><space> :noh<cr>

" Unmap arrows
nnoremap <Up> :echomsg "use k"<cr>
nnoremap <Down> :echomsg "use j"<cr>
nnoremap <Left> :echomsg "use h"<cr>
nnoremap <Right> :echomsg "use l"<cr>

" Get out of any mode with jk instead of ESC
imap jk <Esc>

" Proper yank with Y
nnoremap Y y$

" Split window in panes
nnoremap <silent> vv <C-w>v
nnoremap <silent> vs <C-w>s

" Split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" fix smartindent issue of unindenting comments
filetype indent on

" enable automatic file detection
filetype on

" Allow syntax highlighting in markdown fenced languages
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']

" Tab options for PHP
augroup vimrc-php
	autocmd!
	autocmd Filetype php setl ts=4 sw=4 noexpandtab
augroup END

" javascript
let g:javascript_enable_domhtmlcss = 1

" vim-javascript
augroup vimrc-javascript
	autocmd!
	autocmd FileType javascript setl ts=2 sw=2 noexpandtab "softtabstop=2
augroup END

" Python
" vim-python
augroup vimrc-python
	autocmd!
	autocmd FileType python setl expandtab sw=4 ts=8
		\ colorcolumn=79 formatoptions+=croq softtabstop=4
		\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

" Syntax highlight
" Default highlight is better than polyglot
let g:polyglot_disabled = ['python']
let python_highlight_all = 1

" jedi-vim
let g:jedi#popup_on_dot = 0
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_definitions_command = "<leader>d"
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>n"
let g:jedi#rename_command = "<leader>c"
let g:jedi#show_call_signatures = "0"
let g:jedi#completions_command = "<C-Space>"
let g:jedi#smart_auto_mappings = 0

" pymode
let g:pymode_lint = 0
let g:pymode_lint_signs = 1
let g:pymode_lint_on_fly = 1
let g:pymode_lint_on_write = 1
let g:pymode_lint_message = 1
let g:pymode_lint_sort = ['E', 'C', 'I']
let g:pymode_lint_cwindow = 0

" ALE (Asynchronous Lint Engine)
let g:ale_completion_enabled = 1
let g:ale_linters = {}

:call extend(g:ale_linters, {'python': ['pylint'], 'php': ['php', 'phpcs']})
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 1
let g:ale_php_phpcs_executable ='/var/www/dcs/vendor/squizlabs/php_codesniffer/bin/phpcs'
let g:ale_php_phpcs_standard = 'MediaWiki'
let g:ale_python_executable='python3'
let g:ale_python_pylint_use_global=1

" vim-airline
" theme and extensions
let g:airline_theme = 'lessnoise'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 0
let g:airline#extensions#obsession#enabled = 1
let g:airline#extensions#virtualenv#enabled = 1
let g:airline_skip_empty_sections = 1

" symbols
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          = '▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline#extensions#ale#error_symbol  = '>>'
  let g:airline#extensions#ale#warning_symbol = '--'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = '⎇'
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''
endif

"" NERDTree configuration
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeWinSize = 30
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
nnoremap <leader>t :NERDTreeToggle<CR>

"" The PC is fast enough, do syntax highlight syncing from start unless 200 lines
augroup vimrc-sync-fromstart
  autocmd!
  autocmd BufEnter * :syntax sync maxlines=200
augroup END

"" Remember cursor position
augroup vimrc-remember-cursor-position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

set autoread

" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" Ack vim configuration for RipGrep
" Use ripgrep for searching
" Options include:
" --vimgrep -> Needed to parse the rg response properly for ack.vim
" --type-not sql -> Avoid huge sql file dumps as it slows down the search
" --smart-case -> Search case insensitive if all lowercase pattern, Search case sensitively otherwise
let g:ackprg = 'rg --vimgrep --type-not sql --smart-case'

" Auto close the Quickfix list after pressing '<enter>' on a list item
let g:ack_autoclose = 1

" Any empty ack search will search for the word the cursor is on
let g:ack_use_cword_for_empty_search = 1

" Don't jump to first match
cnoreabbrev Ack Ack!

" Maps <leader>r so we're ready to type the search keyword
nnoremap <leader>r :Ack!<Space>

" Maps <leader>s to do a grep for the searched word
nnoremap <leader>s :AckFromSearch!<Space>

" Navigate quickfix list with ease
nnoremap <silent> [q :cprevious<CR>
nnoremap <silent> ]q :cnext<CR>

" Easymotion configuration
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)

" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)

" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

" Rainbow parentheses
let g:rainbow_active = 1

" Add format option 'w' to add trailing white space, indicating that paragraph
" continues on next line. This is to be used with mutt's 'text_flowed' option.
augroup mail_trailing_whitespace " {
    autocmd!
    autocmd FileType mail setl formatoptions+=w spell wrap linebreak
augroup END " }

" Limelight
map <Leader>f :Limelight 0.7<CR>
map <Leader>u :Limelight!<CR>
nmap <Leader>f :Limelight 0.7<CR>
nmap <Leader>u :Limelight!<CR>

" Start of Vim-Plug manager
call plug#begin()

" Themes and Colors
Plug 'ajh17/spacegray.vim'
Plug 'jaredgorski/SpaceCamp/'
Plug 'cocopon/iceberg.vim'
Plug 'luochen1990/rainbow'
Plug 'amadeus/vim-evokai'
Plug 'vim-scripts/Zenburn'
Plug 'jonathanfilip/vim-lucius'
Plug 'tomasr/molokai'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'mhinz/vim-janah'
Plug 'joshdick/onedark.vim'
Plug 'vim-airline/vim-airline/'
Plug 'vim-airline/vim-airline-themes'
Plug 'ErichDonGubler/vim-sublime-monokai'
Plug 'arcticicestudio/nord-vim'
Plug 'arzg/vim-colors-xcode'
Plug 'junegunn/limelight.vim'

" PHP specific
Plug '2072/PHP-Indenting-for-VIm' " I believe vim-polyglot already does this
" Plug 'lvht/phpcd.vim', { 'for': 'php', 'do': 'composer install' } " Another completion framework
" All those plugins below have a lot of requirements,
" make sure to go through their repos after Plugging them
Plug 'ncm2/ncm2' " Completion framework
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc' " To add support for vim8 * Requires pip3 install pynvim
Plug 'phpactor/phpactor', {'do': 'composer install', 'for': 'php'}
Plug 'phpactor/ncm2-phpactor' " Connect PHPActor and ncm2

autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect

" Python specific
Plug 'davidhalter/jedi-vim'
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'Vimjas/vim-python-pep8-indent'

" Git specific
Plug 'airblade/vim-gitgutter'
Plug 'itchyny/vim-gitbranch'

" Generic
Plug 'sheerun/vim-polyglot' " multilang syntax highlight
Plug 'dense-analysis/ale'
Plug 'majutsushi/tagbar' " browse tree of tags of current file
Plug 'tpope/vim-commentary' " comment stuff easily
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'tpope/vim-obsession' " Better session management
Plug 'mileszs/ack.vim'
Plug 'moll/vim-bbye'
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-repeat'
Plug 'ncm2/ncm2-path'
Plug 'ncm2/ncm2-bufword'
Plug 'pearofducks/ansible-vim'
Plug 'chikamichi/mediawiki.vim' "syntax highlight for Wikitext

call plug#end()

colorscheme xcodedarkhc " default coloscheme at the botton to ensure it loads
